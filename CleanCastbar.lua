CleanCastbar = {}

function CleanCastbar.Initialize()
    local scale = WindowGetScale("LayerTimerWindowCastTimer")
    DestroyWindow("LayerTimerWindowCastTimer")
    CreateWindowFromTemplate("LayerTimerWindowCastTimer", "CleanCastbar", "LayerTimerWindow")
    WindowSetShowing("LayerTimerWindowCastTimer", false)
    WindowSetScale("LayerTimerWindowCastTimer", scale)
    LayerTimerWindow.CAST_BAR_NORMAL_TINT = { r=255, g=255, b=255 }
    LayerTimerWindow.CAST_BAR_FAIL_TINT = { r=150, g=0, b=150 }
end