<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="CleanCastbar" version="1.2" date="24/10/2008" >

		<Author name="Aiiane" email="aiiane@aiiane.net" />
		<Description text="A cleaner castbar." />
        
        <Dependencies>
            <Dependency name="EA_CastTimerWindow" />
            <Dependency name="EATemplate_ParchmentWindowSkin" />
        </Dependencies>
        
		<Files>
            <File name="CleanCastbar.xml" />
            <File name="CleanCastbar.lua" />
		</Files>
		
		<OnInitialize>
            <CallFunction name="CleanCastbar.Initialize" />
        </OnInitialize>
		<OnUpdate/>
		<OnShutdown/>
		
	</UiMod>
</ModuleFile>
